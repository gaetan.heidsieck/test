package de.grogra.ext.x3d.util;

import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;

public class ElevationGridComputation {
	
	final private int xDimension, zDimension;
	final private float xSpacing, zSpacing;
	final private float[] height;
	final private float creaseAngle;
	final private Vector3f dir0, dir1;
	final private Vector3f vertex0, vertex1, vertex2, vertex3, vertex4, vertex5;
	final private Vector3f normalUL, normalLR, normalPreX, normalPreZ, normalSucX, normalSucZ;
	
	public ElevationGridComputation(int xDimension, int zDimension, float xSpacing, float zSpacing,
			float[] height, float creaseAngle) {
		this.xDimension = xDimension;
		this.zDimension = zDimension;
		this.xSpacing = xSpacing;
		this.zSpacing = zSpacing;
		this.height = height;
		this.creaseAngle = creaseAngle;
		this.dir0 = new Vector3f();
		this.dir1 = new Vector3f();
		this.vertex0 = new Vector3f();
		this.vertex1 = new Vector3f();
		this.vertex2 = new Vector3f();
		this.vertex3 = new Vector3f();
		this.vertex4 = new Vector3f();
		this.vertex5 = new Vector3f();
		this.normalUL = new Vector3f();
		this.normalLR = new Vector3f();
		this.normalPreX = new Vector3f();
		this.normalPreZ = new Vector3f();
		this.normalSucX = new Vector3f();
		this.normalSucZ = new Vector3f();
		
	}

	public void getVertex(final Tuple3f vertex, int i, int j) {
		vertex.x =  xSpacing * (i + 0);
		vertex.y = -zSpacing * (j + 0);
		vertex.z = height[(i + 0) + (j + 0) * xDimension];
	}
	
	public void getNormal(final Vector3f normal, final Tuple3f vertex0, final Tuple3f vertex1, final Tuple3f vertex2) {
		dir0.x = vertex1.x - vertex0.x;
		dir0.y = vertex1.y - vertex0.y;
		dir0.z = vertex1.z - vertex0.z;

		dir1.x = vertex2.x - vertex0.x;
		dir1.y = vertex2.y - vertex0.y;
		dir1.z = vertex2.z - vertex0.z;
		
		normal.cross(dir1, dir0);
		normal.normalize();
	}
	
	public void getNormals(final Vector3f normal0, final Vector3f normal1, int i, int j) {
		getULNormal(normal0, i + 0, j + 0);
		getLRNormal(normal1, i + 0, j + 0);
	}
	
	public void getLRNormal(final Vector3f normal, int i, int j) {
		getVertex(vertex3, i + 1, j + 1);
		getVertex(vertex4, i + 0, j + 1);
		getVertex(vertex5, i + 1, j + 0);
		
		getNormal(normal, vertex3, vertex4, vertex5);
	}
	
	public void getULNormal(final Vector3f normal, int i, int j) {
		getVertex(vertex0, i + 0, j + 0);
		getVertex(vertex1, i + 1, j + 0);
		getVertex(vertex2, i + 0, j + 1);
		
		getNormal(normal, vertex0, vertex1, vertex2);
	}
	
	public void getCreaseNormal(final Vector3f normal0, Vector3f normal1, Vector3f normal2,
			Vector3f normal3, Vector3f normal4, Vector3f normal5, int i, int j) {
		
		getNormals(normalUL, normalLR, i, j);
		
		if ((i > 0) && (j > 0))
//			getLRNormal(normalPreX, i - 1, j + 0);
			getLRNormal(normalPreX, i - 1, j - 1);
		else
			normalPreX.set(normalUL);
			
//		if (j > 0)
//			getLRNormal(normalPreZ, i + 0, j - 1);
//		else
//			normalPreZ.set(normalUL);
		
		if ((i < xDimension - 2) && ( j < zDimension - 2))
//			getULNormal(normalSucX, i + 1, j + 0);
			getULNormal(normalSucX, i + 1, j + 1);
		else
			normalSucX.set(normalLR);
		
//		if (j < zDimension - 2)
//			getULNormal(normalSucZ, i + 0, j + 1);
//		else
//			normalSucZ.set(normalLR);
		
		// set normal 0
		normal0.set(normalUL);
		if (normalPreX.angle(normalUL) < creaseAngle) {
			// smooth
			normal0.set((normalPreX.x + normal0.x),
						(normalPreX.y + normal0.y),
						(normalPreX.z + normal0.z));
			normal0.normalize();
		}
//		if (normalPreZ.angle(normalUL) < creaseAngle) {
//			// smooth
//			normal0.set((normalPreZ.x + normal0.x) / 2f,
//						(normalPreZ.y + normal0.y) / 2f,
//						(normalPreZ.z + normal0.z) / 2f);
//			normal0.normalize();
//		}
		
		// set normal 3
		normal3.set(normalLR);
		if (normalSucX.angle(normalLR) < creaseAngle) {
			// smooth
			normal3.set((normalSucX.x + normal3.x),
						(normalSucX.y + normal3.y),
						(normalSucX.z + normal3.z));
			normal3.normalize();
		}
//		if (normalSucZ.angle(normalLR) < creaseAngle) {
//			// smooth
//			normal3.set((normalSucZ.x + normal3.x) / 2f,
//						(normalSucZ.y + normal3.y) / 2f,
//						(normalSucZ.z + normal3.z) / 2f);
//			normal3.normalize();
//		}
		
		// set normals at same quadric
		if (normalUL.angle(normalLR) < creaseAngle) {
			normal1.set((normalUL.x + normalLR.x) / 2f,
						(normalUL.y + normalLR.y) / 2f,
						(normalUL.z + normalLR.z) / 2f);
			normal1.normalize();
			normal2.set(normal1);
			normal4.set(normal1);
			normal5.set(normal1);
		}
		else {
			normal1.set(normalUL);
			normal2.set(normalUL);
			normal4.set(normalLR);
			normal5.set(normalLR);
		}
		
	}
	
}
