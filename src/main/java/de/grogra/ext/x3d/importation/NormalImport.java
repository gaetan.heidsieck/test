package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.NormalDocument.Normal;

public class NormalImport {

	public static float[] createInstance(Normal normal) {
		float[] v = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (normal.isSetUSE()) {
			v = (float[]) referenceMap.get(normal.getUSE());
		}
		else {
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (normal.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, normal);
			}
			
			v = setValues(normal, finalValueMap);
			if (normal.isSetDEF()) {
				referenceMap.put(normal.getDEF(), v);
			}				
		}
	
		return v;
	}
	
	private static float[] setValues(Normal normal, HashMap<String, String> valueMap) {
		// color
		float[] v;
		String vString = valueMap.containsKey("vector") ? valueMap.get("vector") : normal.getVector();
		v = Util.splitStringToArrayOfFloat(vString);
		return v;
	}
	
}
